# ACERCA DE

Esta app obtiene la ubicación de tu dispositivo y verifica si pertenece aun determinada area

## CORRIENDO EL PROYECTO

Para correr esta app la puedes descargar el apk desde la sección release de este repositorio 

Para correrlo dese del código 

> Para correr este repositorio necesitas una llave de google maps
> Para conseguirla puedes seguir la siguiente pagina https://cloud.google.com/maps-platform/

> Agrega tu key en el archivo `android/local.properties` de la siguiente forma

```
MAPS_API_KEY=tu-api-key
```

- clonar el repositorio
- correr el comando `flutter pub get`
- conectar un dispositivo android por adb 
- correr el comando `flutter run`



