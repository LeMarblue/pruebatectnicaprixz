import 'package:flutter/material.dart';
import 'package:prueba_tecnica/src/core/routes/app_router.dart';
import 'package:prueba_tecnica/src/core/routes/named_routes.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final router = AppRouter();
    return MaterialApp(
      routes: router.routes,
      initialRoute: NamedRoutes.homeScreen,
      debugShowCheckedModeBanner: false,
    );
  }
}
