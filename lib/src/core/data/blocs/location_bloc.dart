import 'dart:async';
import 'package:geolocator/geolocator.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:poly/poly.dart';

class LocationBloc {
  final _positionStreamController = StreamController<Position>();
  Position currentPosition;
  final _isInAreaStreamController = StreamController<bool>();

  var isInArea1 = false;
  var isInArea2 = true;

  Stream<Position> get positionStream => _positionStreamController.stream;
  Stream<bool> get isInAreaStream => _isInAreaStreamController.stream;

  Future<void> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      _positionStreamController.addError('Location services are disabled.');
      return null;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.deniedForever) {
        _positionStreamController.addError(
          'Location permissions are permanently denied, we cannot request permissions.',
        );
        return null;
      }
      if (permission == LocationPermission.denied) {
        _positionStreamController.addError(
          'Location permissions are denied',
        );
        return null;
      }
    }

    currentPosition = await Geolocator.getCurrentPosition();

    _positionStreamController.add(currentPosition);
  } //Future

  Polygon createArea(List<String> match) {
    var coordinates = match.sublist(1, match.length - 1);
    var pointList = coordinates.map((element) {
      var pointXY = element.split(',');
      var coordinateX = num.parse(pointXY[0]);
      var coordinateY = num.parse(pointXY[1]);
      var pointCoor = Point(
        coordinateX,
        coordinateY,
      );
      return pointCoor;
    }).toList();
    return Polygon(pointList);
  }

  Future<void> approachVerification() async {
    var fileAsString =
        await rootBundle.loadString('assets/capa_sin_nombre.kml');
    var regExp = RegExp(
      r'(?<=coordinates\>)([.\s-\,\.]|[0-9])+(?=\<\/coordinates)',
      multiLine: true,
      caseSensitive: false,
      dotAll: true,
    );
    var matches = regExp.allMatches(fileAsString).toList();
    var match1 = matches.first.group(0).replaceAll(' ', '').split(',0\n');
    var match2 = matches[1].group(0).replaceAll(' ', '').split(',0\n');
    var area1 = createArea(match1);
    var area2 = createArea(match2);
    var pointsLocation = Point(
      currentPosition.longitude,
      currentPosition.latitude,
    );
    isInArea1 = area1.isPointInside(pointsLocation);
    isInArea2 = area2.isPointInside(pointsLocation);
    if (isInArea1 || isInArea2) {
      _isInAreaStreamController.add(true);
      return null;
    } else {
      _isInAreaStreamController.add(false);
      return null;
    }
  }

  void dispose() {
    _positionStreamController.close();
    _isInAreaStreamController.close();
  }
}
