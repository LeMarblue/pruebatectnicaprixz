import 'package:flutter/widgets.dart';
import 'package:prueba_tecnica/src/core/routes/named_routes.dart';
import 'package:prueba_tecnica/src/features/home_page/home_screen.dart';

class AppRouter {
  Map<String, WidgetBuilder> get routes => {
        NamedRoutes.homeScreen: (context) => HomeScreen(),
      };
}
