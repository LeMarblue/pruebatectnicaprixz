import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:prueba_tecnica/src/core/data/blocs/location_bloc.dart';
import 'package:geolocator/geolocator.dart';

class HomeScreen extends StatelessWidget {
  final _controllerCompleter = Completer<GoogleMapController>();

  final mexico = CameraPosition(
    target: LatLng(19.411237, -99.0640467),
    zoom: 10,
  );

  Future<void> updatePosition(CameraUpdate newPosition) async {
    final controller = await _controllerCompleter.future;
    controller.animateCamera(newPosition);
  }

  @override
  Widget build(BuildContext context) {
    final locationBloc = LocationBloc();

    locationBloc.isInAreaStream.listen((event) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Stack(
            alignment: Alignment.center,
            children: [
              BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: 5.0,
                  sigmaY: 5.0,
                ),
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.05,
                  color: Colors.black.withOpacity(0.0),
                ),
              ),
              Text(
                event == true
                    ? 'You are within the area'
                    : 'You are out of the area',
                style: TextStyle(fontSize: 18),
              )
            ],
          ),
          elevation: 0,
          backgroundColor: event == true
              ? Colors.green.withOpacity(0.6)
              : Colors.red[800].withOpacity(0.6),
        ),
      );
    });

    return Scaffold(
      body: StreamBuilder<Position>(
        stream: locationBloc.positionStream,
        builder: (context, snapshot) {
          var markers = <Marker>{};
          CameraUpdate myPosition;
          if (snapshot.hasData) {
            final coordinates = snapshot.data;
            final currentLocation =
                LatLng(coordinates.latitude, coordinates.longitude);
            final marker = Marker(
              markerId: MarkerId('Current Location'),
              position: currentLocation,
            );
            markers.add(marker);
            myPosition = CameraUpdate.newCameraPosition(
              CameraPosition(
                target: currentLocation,
                zoom: 14.5,
              ),
            );
            updatePosition(myPosition);
          }
          return GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: mexico,
            onMapCreated: (GoogleMapController controller) {
              _controllerCompleter.complete(controller);
            },
            markers: markers,
            zoomControlsEnabled: false,
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        elevation: 0,
        child: Icon(
          Icons.my_location,
        ),
        onPressed: () async {
          await locationBloc.determinePosition();
          locationBloc.approachVerification();
        },
      ),
    );
  }
}
